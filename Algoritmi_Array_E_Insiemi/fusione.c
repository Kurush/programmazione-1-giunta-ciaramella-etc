#include <stdio.h>
#include <stdlib.h>

void fusione_AI(int n_a,int a[],int n_b,int b[],int n_c,int c[]);
void visualizza_aI(int n,int ArrayA[]);
void lettura_aI(int n,int arrayG[]);
int main()
{
 int n_a=100,n_b=100,n_c=200;
 int a[n_a];
 int b[n_b];
 int c[n_c];

 printf("Inserisci la lunghezza di A e di B :");
 scanf("%d %d",&n_a,&n_b);
 n_c = n_a + n_b;

 printf("Inserisci gli elementi di A:\n");
 lettura_aI(n_a,a);
  printf("Inserisci gli elementi di A:\n");
 lettura_aI(n_b,b);


 fusione_AI(n_a,a,n_b,b,n_c,c);


printf("L'Array C vale: \n");
visualizza_aI(n_c,&c[0]);







    return 0;
}

//Algoritmo fusione array
void fusione_AI(int n_a,int a[],int n_b,int b[],int n_c,int c[]){ //3 array e i rispettivi size
int i_a=0;
int i_b=0;
int i_c=0; //Questi sono gli indici che verranno usati per confrontare gli array

for(i_c=0;i_c<n_a+n_b;i_c++){ //il for scorre l'array c che è grande n_a + n_b
    if(i_a < n_a && i_b < n_b){
        if(a[i_a] < b[i_b]){
            c[i_c]=a[i_a];
            i_a++;
        }else{
         c[i_c]=b[i_b];
         i_b++;
         }

        }else if(i_b >= n_b){
        c[i_c] = a[i_a];
        i_a++;
        }else{
        c[i_c] = b[i_a];
        i_b++;
        }

    }

}
/* COMMENTO ALGORITMO DI FUSIONE
In input sono richiesti i tre array con i rispettivi size, è un void quindi andrà a modificare i valori direttamente
il ciclo for fa scorrere l'array C
A questo punto si apre un if a tre vie , nel primo if è innestato un if-else
il costrutto di controllo if else agisce in questo modo:
rimpiazza il più piccolo tra a e b e aumenta il rispettivo indice dell'array
l'if-else tiene in considerazione il caso in cui l'array b è stato già interamente inserito nell'array c e quindi
assegna semplicemente i rimanenti aumentando l'indice
l'else considera il caso opposto a quell'dell'if-else

la complessità di tempo è T(n) = n_a + n_b confronti
la complessità di spazio è S(n+m) = (n+m)*2



*/


//Algoritmo lettura da tastiera di un array di interi

void lettura_aI(int n,int arrayG[]){
int i;
for(i=0;i<n;i++){
scanf("%d",&arrayG[i]);
}

}

//Algoritmo visualizzazione da tastiera
void visualizza_aI(int n,int ArrayA[]){
int i=0;
for (i=0;i<n;i++){
    printf("%d ",ArrayA[i]);
    }
}
