#include <stdio.h>
#include <stdlib.h>
void massimo_minimo_AI(int a[],int n,int *max,int *min);
int main()
{
    int a[10]={1,3,21,23,5,32,38,19,11,17};
    int max,min; //Attenzione : dichiarare dei puntatori e poi richiamarli tramite l'indirizzo è un errore
    /*Infatti questo significa che la funzione cercherà di accedere
    all'indirizzo dei puntatori e non all'indirizzo delle variabili
    */

    massimo_minimo_AI(a,10,&max,&min);

    printf("L'elemento massimo é : %d",max);
    printf("\nL'elemento minimo è : %d",min);
    return 0;
}


//Algoritmo per il calcolo dell'elemento minimo e massimo contemporaneamente;
//Complessità di tempo : T(n) = 2(n-1) confronti
//Complessità di spazio : S(n) = n;
void massimo_minimo_AI(int a[],int n,int *max,int *min){
    int i;
    *max = a[0];
    *min = a[0];

    for(i=1;i<n;i++){

    if(a[i]>*max)
    *max = a[i];

    if(a[i]<*min)
    *min = a[i];
    }
}
