#include <stdio.h>
#include <stdlib.h>
//________MAIN________


int uguaglianza_AI(int n,int *a,int *b);
int main()
{
    int a[5] = {4,8,3,16,20};
    int b[5] = {4,8,12,16,20};

    if(uguaglianza_AI(5,a,b))
        printf("Uguali!\n");
    else
        printf("Diseguali!\n");

    return 0;
}

//Algoritmo di uguaglianza array
    int uguaglianza_AI(int n,int *a,int *b){
    int i;
    for(i=0;i<n;i++){
        if(a[i]!=b[i])
        return 0;
        }
    return 1;
    }

/*
Confronta due array e ritorna 1 se sono uguali, 0 se sono diseguali

il costo di T(n) =  al più n confronti
il costo di S(n) =  2n


*/
