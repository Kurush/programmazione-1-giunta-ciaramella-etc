/* MAIN (TRASCURABILE)*/

int main(){
int i=0,n=0,a[n];
int chiave;

printf("inserisci la lunghezza dell'array di a");
scanf("%d",&n);

printf("inserisci una chiave");
scanf("%d",&chiave);

printf("Inserisci gli elementi dell'array a");
    for(i=0;i<n;i++){
    scanf("%d",&a[i]);
    }
    
if(appartiene(chiave,a,n){
    printf("Appartiene!");
}else{
    printf("Non appartiene!");
    }
    return 0;
}



/* function che determina l’appartenenza di un
numero(chiave) a un array (a) di size n
--algoritmo di ricerca sequenziale--
*/

int appartiene(int chiave,int a[],int n){
int i=0
for(i=0; i<n; i++){
  if (chiave == a[i])
    return 1;
    
    
}
return 0;
}

/* 
T(n) = al più O(n) confronti 
infatti nel caso migliore la chiave si trova al primo posto nell'array e in tal caso n=1 , nel caso peggiore per esempio in un array di 10 , sarà n=10
Non c'è molto da commentare,l'algoritmo è molto semplice ed è utilizzato in tutti gli algoritmi dell'argomento 9 sugli insiemi
*/