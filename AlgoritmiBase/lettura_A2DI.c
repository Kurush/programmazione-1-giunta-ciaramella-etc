#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a[3][3];
    int i,j;

    printf("Inserisci gli elementi dell'array\n");
    lettura_A2DI(3,3,a,3);

    for(i=0;i<3;i++){
        printf("\n");
        for(j=0;j<3;j++)
            printf("%d ",a[i][j]);

    }
    return 0;
}

void lettura_A2DI(int n,int m,int a[][m]){
    int i,j;
    for(i=0;i<n;i++){
        for(j=0;j<m;j++)
            scanf("%d",&a[i][j]);
    }

}
