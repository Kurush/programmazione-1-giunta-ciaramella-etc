#include <stdio.h>
#include <stdlib.h>


int massimo_ArrayI(int n,int a[]);
int main()
{
    int a[10] = {1,5,15,3,22,21,18,29,10,14};

    printf("L'elemento massimo dell'array è: %d",massimo_ArrayI(10,a));
    return 0;
}

int massimo_ArrayI(int n,int a[]){
    int i;
    int  massimo = a[0];
    for(i=1;i<n;i++){
        if(massimo<a[i])
        massimo = a[i];
    }
    return massimo;
} //T(n) = n-1;