#include <stdio.h>
#include <stdlib.h>


//Main
int main()
{
int A[3][3]= {{1,2,3},
              {4,5,6},
              {7,8,9} };

 visualizza_A2D(3,3,A);

    return 0;
}

//stampa a video un array bidimensionale

int visualizza_A2D(int n,int m,int A[][m]){
//Nella dichiarazione dell'array 2D va specificato il numero di colonne
int i,j;

for (i=0;i<n;i++){

    for(j=0;j<m;j++){
    printf("%d ",A[i][j] );
    }
    printf("\n");
}
printf("\n");
}

//Stampa a video un array bidimensionale