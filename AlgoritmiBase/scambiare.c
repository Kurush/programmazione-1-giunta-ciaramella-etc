#include <stdio.h>
#include <stdlib.h>


//_________MAIN________//
int main()
{
   int a;
   int b;
   printf("Inserisci i valori di a \n");
   scanf("%d",&a);
   printf("Inserisci i valori di b \n");
   scanf("%d",&b);


   scambiare(&a,&b);
   printf("\n\n\n\n\n\n%d\n%d",a,b);

    return 0;
}

//Algoritmo di SWAP, che scambia i valori di due variabili passate tramite puntatore

void scambiare(int *a,int *b){
int temp;
temp = *a;
*a = *b;
*b = temp;

}

//L'algoritmo scambiare, scambia i valori di due variabili passate come puntatori, tramite l'utlizzo di  una terza variabile;