#include <stdio.h>
#include <stdlib.h>


int minimo_ArrayI(int n,int a[]);
int main()
{
    int a[10] = {1,5,15,3,22,21,18,29,10,14};

    printf("L'elemento minimo dell'array è: %d",minimo_ArrayI(10,a));
    return 0;
}

int minimo_ArrayI(int n,int a[]){
    int i;
    int  minimo = a[0];
    for(i=1;i<n;i++){
        if(minimo>a[i])
        minimo = a[i];
    }
    return minimo;
}