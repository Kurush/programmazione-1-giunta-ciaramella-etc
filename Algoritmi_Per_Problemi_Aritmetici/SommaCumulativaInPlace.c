#include <stdio.h>
#include <stdlib.h>

void visualizza_aI(int n,int ArrayA[]);
void lettura_AI(int n,int Array[]);
void somma_cumulativa(int n,int a[]);
int main()
{
    int n=0,a[n];
    printf("Inserisci la lunghezza dell'array: ");
    scanf("%d",&n);

    printf("Inserisci i valori dell'array\n");
    lettura_AI(n,a);

    somma_cumulativa(n,a);
    visualizza_aI(n,a);
    return 0;
}
//Algoritmo per il calcolo della somma cumulativa (IN PLACE =/= DA QUELLO DELLE SLIDE)
void somma_cumulativa(int n,int a[]){
    int i;
    int ptemp;
    for(i=1;i<n;i++){
    ptemp = a[i-1];
    a[i] = a[i] + ptemp;
    }
}
//Algoritmo lettura array
void lettura_AI(int n,int Array[]){
    int i;
    for(i=0;i<n;i++){
    printf("Inserimento numero %d°\n",i+1);
    scanf("%d",&Array[i]);
    }
}
//Algoritmo visualizza array
void visualizza_aI(int n,int ArrayA[]){
    int i=0;
    for (i=0;i<n;i++){
    printf("%3d",ArrayA[i]);
    }
}