#include <stdio.h>
#include <stdlib.h>

int fattoriale_i(int n);
int main()
{
    int n;
    printf("Inserisci n: ");
    scanf("%d",&n);

    printf("Il fattoriale vale %d",fattoriale_i(n));
    return 0;
}

//Algoritmo per il calcolo del fattoriale
int fattoriale_i(int n){
int i;
int fatt=1;
if(n>0){
    for(i=1;i<=n;i++){
    fatt = fatt * i;
    }
}
return fatt;
}