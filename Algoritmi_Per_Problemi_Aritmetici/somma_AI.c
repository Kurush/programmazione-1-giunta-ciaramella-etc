include <stdio.h>
#include <stdlib.h>

int main()
{
    int n=0,i;
    int a[n];
    printf("Inserisci la lunghezza dell'array: ");
    scanf("%d",&n);
    printf("Inserisci i valori dell'array\n");
    for(i=0;i<n;i++){
    scanf("%d",&a[i]);
    }

    printf("La somma vale: %d",somma_array(n,a) );

    return 0;
}

//Algoritmo per la somma degli elementi dell'array
int somma_array(int n,int a[]){
    int i,somma=0;

    for(i=0;i<n;i++){
        somma = somma + a[i];
    }
    
    return somma;
}
/*
Questo algoritmo somma gli elementi presenti all'interno dell'array A
la complessità di T(n) = n somme
la complessità di S(n) = n
*/